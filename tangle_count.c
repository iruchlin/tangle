#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <mpi.h>

unsigned long long reverse_bit(unsigned long long idx, const unsigned int n) {
    unsigned long long xdi = 0;

    for(unsigned int k = 0; k < n; k++) {
        xdi = (xdi << 1) + (idx & 1);
        idx >>= 1;
    }

    return xdi;
}

unsigned long long cycle_shift_right(const unsigned long long idx, const unsigned int k, const unsigned int n) {
    return (idx >> k) | ((idx << (n - k)) & (((unsigned long long)1 << n) - 1));
}

bool is_tangle(unsigned long long idx, const unsigned int n, bool *p) {
    int x = 1;
    int y = 0;
    int u = 0;
    int v = 0;
    int w = 1;
    int go = n;
    const int n2 = n * n;
    int ind = n2 + 1;
    while(go && !p[ind]) {
        p[ind] = 1;
        const int a = w * (v - y);
        const int b = w * (x - u);
        if(!(idx & 1)) {
            u = 2 * x - u;
            v = 2 * y - v;
            w = -w;
        }
        x = a + u;
        y = b + v;
        idx >>= 1;
        ind = x + n * y + n2;
        go--;
    }

    return (x == 1) && (y == 0) && (u == 0) && (v == 0) && (w == 1) && (go == 0);
}

unsigned long long find_tangles(const unsigned int n, const unsigned long long idx_min, const unsigned long long idx_0, const unsigned long long idx_1) {
    bool p[n + 2 * n * n];
    unsigned long long tangle_count = 0;
    unsigned long long idx = idx_0;
    while(idx < idx_1) {
        const unsigned long long tmp = idx & (idx << 1) & (idx << 2);
        if(tmp) {
            const int b = __builtin_ctzll(tmp);
            idx = ((idx + tmp) >> b) << b;
        }
        else {
            const int hw = __builtin_popcountll(idx);
            memset(p, 0, sizeof(p));
            if(!(hw & 1) && (hw >> 2) && is_tangle(idx, n, p)) {
                unsigned long long min_idx_shift = idx_1;
                const unsigned long long xdi = reverse_bit(idx, n);
                for(unsigned int k = 0; k < n; k++) {
                    const unsigned long long idx_s = cycle_shift_right(idx, k, n);
                    const unsigned long long xdi_s = cycle_shift_right(xdi, k, n);
                    if((min_idx_shift > idx_s) && (idx_s > idx_min)) {
                        min_idx_shift = idx_s;
                    }
                    if((min_idx_shift > xdi_s) && (xdi_s > idx_min)) {
                        min_idx_shift = xdi_s;
                    }
                }
                if(idx == min_idx_shift) {
                    tangle_count++;
                }
            }
            idx++;
        }
    }

    return tangle_count;
}

int main(int argc, char **argv) {

    int rank, root_process, size;
    //MPI_Status status;

    /* Let process 0 be the root process. */

    root_process = 0;

    /* Now replicate this process to create parallel processes. */

    MPI_Init(&argc, &argv);

    /* Find out MY process ID, and how many processes were started. */

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);


    const unsigned int c = atoi(argv[1]);
    const unsigned int n = 4 * c;

    const unsigned long long idx_min = (unsigned long long)1 << (n - 2);

    unsigned long long idx_max = 3;
    for(unsigned int i = 0; i < n / 3; i++) {
        idx_max = (idx_max << 3) + 3;
    }
    idx_max >>= (n / 3 + 1) * 3 - n;
    idx_max++;

    if(rank == root_process) {
        printf("idx_min = %llu\nidx_max = %llu\n", idx_min, idx_max);
    }

    const unsigned long long idx_delta = idx_max - idx_min;
    const unsigned long long idx_0 = idx_min + idx_delta * rank / size;
    const unsigned long long idx_1 = idx_min + idx_delta * (rank + 1) / size;

    printf("rank %d, idx_0 %llu, idx_1 %llu\n", rank, idx_0, idx_1);

    const time_t start = time(NULL);
    const unsigned long long tangle_count = find_tangles(n, idx_min, idx_0, idx_1);
    printf("time %ld, tangles %llu\n", time(NULL) - start, tangle_count);

    unsigned long long total_tangle_count;
    MPI_Reduce(&tangle_count, &total_tangle_count, 1, MPI_LONG_LONG, MPI_SUM, root_process, MPI_COMM_WORLD);

    if(rank == root_process) {
        printf("class %d\nlength %d\nn_tangles %llu\n", c, n, total_tangle_count);
    }

    MPI_Finalize();

    return 0;
}
