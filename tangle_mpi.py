import sys
import numpy as np
import numba as nb
from time import time
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

c = int(sys.argv[1])
n = 4 * c
m = 2 ** n - 1

idx_min = 2 ** (n - 2)
idx_max = int('0b' + ('011' * ((n // 3) + 1))[:n], 2) + 1
idx_interval = np.linspace(idx_min, idx_max, num=(size+1)).astype(np.int_)
idx_0 = idx_interval[rank]
idx_1 = idx_interval[rank+1]

m1 = 0x5555555555555555
m2 = 0x3333333333333333
m4 = 0x0f0f0f0f0f0f0f0f
h01 = 0x0101010101010101


@nb.njit(fastmath=True)
def popcount_a(x):
    x -= (x >> 1) & m1
    x = (x & m2) + ((x >> 2) & m2)
    x = (x + (x >> 4)) & m4
    x += x >> 8
    x += x >> 16
    x += x >> 32
    return x & 0x7f


@nb.njit(fastmath=True)
def popcount_b(x):
    x -= (x >> 1) & m1
    x = (x & m2) + ((x >> 2) & m2)
    x = (x + (x >> 4)) & m4
    return (x * h01) >> 56


@nb.njit(fastmath=True)
def div_4(x):
    return ((x >> 2) << 2) == x


@nb.njit(fastmath=True)
def is_tangle(idx):
    x, y, u, v, w = 1, 0, 0, 0, 1
    p = np.empty(n, dtype=np.int_)
    for j in range(n):
        p[j] = x + n * y
        a = w * (v - y)
        b = w * (x - u)
        if not idx & 1:
            u = 2 * x - u
            v = 2 * y - v
            w = -w
        x = a + u
        y = b + v
        idx >>= 1
    return x == 1 and y == 0 and u == 0 and v == 0 and w == 1 and np.unique(p).size == n


@nb.njit(fastmath=True)
def find_tangles():
    tangle_list = []
    for idx in range(idx_0, idx_1):
        hw = popcount_b(idx)
        if (not hw & 1) and (hw >= 4) and (not idx & (idx << 1) & (idx << 2)) and is_tangle(idx):
            tangle_list.append(idx)
    return tangle_list


@nb.njit()
def reverse_bit(t):
    r = 0
    for _ in range(n):
        r = (r << 1) + (t & 1)
        t >>= 1
    return r


@nb.njit()
def cycle_shift_right(t, j):
    return (t >> j) | ((t << (n - j)) & m)


def unique_tangles(full_tangle_list):
    unique_tangle_list = []
    idx_list = set([])
    idx_roll = np.empty((n, 2), dtype=np.int_)
    for idx in full_tangle_list:
        if idx not in idx_list:
            xdi = reverse_bit(idx)
            for j in range(n):
                a = cycle_shift_right(idx, j)
                b = cycle_shift_right(xdi, j)
                idx_list.add(a)
                idx_list.add(b)
                idx_roll[j] = a, b
            unique_tangle_list.append(np.min(idx_roll))
    # return np.sort(np.array(unique_tangle_list))
    return unique_tangle_list


start = time()
t_list = find_tangles()
print('rank:', rank, ', tangles:', len(t_list), 'time:', time()-start)
u_list = unique_tangles(t_list)
master = []
master = comm.gather(u_list, root=0)

if rank == 0:
    u_tangles = unique_tangles([idx for sublist in master for idx in sublist])
    print('class', c)
    print('length', n)
    print('n_tangles', len(u_tangles))
    # print([[bin(t), popcount_b(t)] for t in u_tangles])
