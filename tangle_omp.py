import sys
import numpy as np
import numba as nb
from time import time


c = int(sys.argv[1])
n = 4 * c
m = 2 ** n - 1

idx_min = 2 ** (n - 2)
idx_max = int('0b' + ('011' * ((n // 3) + 1))[:n], 2) + 1

m1 = 0x5555555555555555
m2 = 0x3333333333333333
m4 = 0x0f0f0f0f0f0f0f0f
h01 = 0x0101010101010101


@nb.njit(fastmath=True)
def popcount(x):
    x -= (x >> 1) & m1
    x = (x & m2) + ((x >> 2) & m2)
    x = (x + (x >> 4)) & m4
    return (x * h01) >> 56


@nb.njit(fastmath=True)
def is_tangle(idx):
    x, y, u, v, w = 1, 0, 0, 0, 1
    p = np.empty(n, dtype=np.int_)
    for j in range(n):
        p[j] = x + n * y
        a = w * (v - y)
        b = w * (x - u)
        if not idx & 1:
            u = 2 * x - u
            v = 2 * y - v
            w = -w
        x = a + u
        y = b + v
        idx >>= 1
    return x == 1 and y == 0 and u == 0 and v == 0 and w == 1 and np.unique(p).size == n


# @nb.njit(parallel=True, fastmath=True)
# def find_tangles():
#     tangle_list = np.zeros(idx_max - idx_min, dtype=np.bool_)
#     for idx in nb.prange(idx_min, idx_max):
#         hw = popcount(idx)
#         if (not hw & 1) and (hw >= 4) and (not idx & (idx << 1) & (idx << 2)) and is_tangle(idx):
#             tangle_list[idx - idx_min] = True
#     return tangle_list


@nb.njit(parallel=True, fastmath=True)
def find_tangles():
    num_threads = int(nb.get_num_threads())
    tangle_list = np.zeros((num_threads, 2 * 10 ** (4 + c - 7) // num_threads), dtype=np.int_)
    print(tangle_list.shape)
    idx_list = np.linspace(0, idx_max - idx_min, num_threads + 1).astype(np.int_)
    for th in nb.prange(num_threads):
        k = 0
        for i in range(idx_list[th], idx_list[th + 1]):
            idx = i + idx_min
            hw = popcount(idx)
            if (not hw & 1) and (hw >= 4) and (not idx & (idx << 1) & (idx << 2)) and is_tangle(idx):
                tangle_list[th, k] = idx
                k += 1
        print('thread', th, ', tangles', k)
    return tangle_list.flatten()


@nb.njit()
def reverse_bit(t):
    r = 0
    for _ in range(n):
        r = (r << 1) + (t & 1)
        t >>= 1
    return r


@nb.njit()
def cycle_shift_right(t, j):
    return (t >> j) | ((t << (n - j)) & m)


# def unique_tangles(full_tangle_list):
#     unique_tangle_list = []
#     idx_list = set([])
#     idx_roll = np.empty((n, 2), dtype=np.int_)
#     for k in np.nonzero(full_tangle_list)[0]:
#         idx = k + idx_min
#         if idx not in idx_list:
#             xdi = reverse_bit(idx)
#             for j in range(n):
#                 a = cycle_shift_right(idx, j)
#                 b = cycle_shift_right(xdi, j)
#                 idx_list.add(a)
#                 idx_list.add(b)
#                 idx_roll[j] = a, b
#             unique_tangle_list.append(np.min(idx_roll))
#     # return np.sort(np.array(unique_tangle_list))
#     return unique_tangle_list


def unique_tangles(full_tangle_list):
    unique_tangle_list = []
    idx_list = set([])
    idx_roll = np.empty((n, 2), dtype=np.int_)
    for idx in full_tangle_list[np.nonzero(full_tangle_list)]:
        if idx not in idx_list:
            xdi = reverse_bit(idx)
            for j in range(n):
                a = cycle_shift_right(idx, j)
                b = cycle_shift_right(xdi, j)
                idx_list.add(a)
                idx_list.add(b)
                idx_roll[j] = a, b
            unique_tangle_list.append(np.min(idx_roll))
    # return np.sort(np.array(unique_tangle_list))
    return unique_tangle_list


start = time()
t_list = find_tangles()
print('time:', time()-start)
print('class', c)
print('length', n)
print('n_tangles', len(unique_tangles(t_list)))
