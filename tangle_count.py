import sys
import numpy as np
import numba as nb
from time import time
from mpi4py import MPI

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

nb.set_num_threads(size)

c = int(sys.argv[1])
n = 4 * c
m = 2 ** n - 1
j = np.arange(n)

idx_min = 2 ** (n - 2)
idx_max = int('0b' + ('011' * ((n // 3) + 1))[:n], 2) + 1
idx_interval = np.linspace(idx_min, idx_max, size + 1).astype(np.int_)
idx_0 = idx_interval[rank]
idx_1 = idx_interval[rank+1]

print('idx_min =', idx_min)
print('idx_max =', idx_max)

# print('011' * ((n // 3) + 1))
# print(('011' * ((n // 3) + 1))[:n])

# im = 3
# for i in range(n // 3):
#     im <<= 3
#     im |= 3
# print(im, bin(im))
# im >>= (n // 3 + 1) * 3 - n
# im += 1
# print(im, bin(im))

m1 = 0x5555555555555555
m2 = 0x3333333333333333
m4 = 0x0f0f0f0f0f0f0f0f
h01 = 0x0101010101010101


@nb.njit(fastmath=True)
def popcount(x):
    x -= (x >> 1) & m1
    x = (x & m2) + ((x >> 2) & m2)
    x = (x + (x >> 4)) & m4
    return (x * h01) >> 56


@nb.njit(fastmath=True)
def reverse_bit(t):
    r = 0
    for _ in range(n):
        r = (r << 1) + (t & 1)
        t >>= 1
    return r


@nb.njit(fastmath=True)
def cycle_shift_right(t):
    return (t >> j) | ((t << (n - j)) & m)


# @nb.njit(fastmath=True)
# def is_tangle(idx, p):
#     x, y, u, v, w = 1, 0, 0, 0, True
#     for k in range(n):
#         p[k] = x + n * y
#         if w:
#             a = v - y
#             b = x - u
#         else:
#             a = y - v
#             b = u - x
#         if not idx & 1:
#             u = 2 * x - u
#             v = 2 * y - v
#             w = not w
#         x = a + u
#         y = b + v
#         idx >>= 1
#     return x == 1 and y == 0 and u == 0 and v == 0 and w and np.unique(p).size == n


@nb.njit(fastmath=True)
def is_tangle(idx, p):
    x, y, u, v, w = 1, 0, 0, 0, 1
    for k in range(n):
        p[k] = x + n * y
        a = w * (v - y)
        b = w * (x - u)
        if not idx & 1:
            u = 2 * x - u
            v = 2 * y - v
            w = -w
        x = a + u
        y = b + v
        idx >>= 1
    return x == 1 and y == 0 and u == 0 and v == 0 and w == 1 and np.unique(p).size == n


# @nb.njit(fastmath=True)
# def is_tangle(idx, n, p, T, v):
#     p[:] = False
#     v[:] = 0
#     v[0] = 1
#     v[5] = 1
#     go = n
#     ind = 1 + n * n
#     while go and not p[ind]:
#         p[ind] = True
#         v = np.dot(T[idx & 15], v)
#         idx >>= 4
#         go -= 4
#         ind = int(v[0] + n * (v[1] + n))
#     return v[0] == 1 and v[1] == 0 and v[2] == 0 and v[3] == 0 and v[4] == 0 and v[5] == 1 and go == 0


# @nb.njit(fastmath=True)
# def is_tangle(idx, p):
#     x, y, u, v, w = 1, 0, 0, 0, 1
#     go = n
#     ind = 1 + n * n
#     while go and not p[ind]:
#         p[ind] = True
#         a = w * (v - y)
#         b = w * (x - u)
#         if not idx & 1:
#             u = 2 * x - u
#             v = 2 * y - v
#             w = -w
#         x = a + u
#         y = b + v
#         idx >>= 1
#         go -= 1
#         ind = x + n * y + n * n
#     return x == 1 and y == 0 and u == 0 and v == 0 and w == 1 and go == 0


@nb.njit(fastmath=True)
def find_tangles():
    # Tp = np.zeros((6, 6))
    # Tm = np.zeros((6, 6))
    #
    # Tp[0, 2] = 1
    # Tp[0, 4] = 1
    # Tp[1, 3] = 1
    # Tp[1, 5] = 1
    # Tp[2, 2] = 1
    # Tp[3, 3] = 1
    # Tp[4, 0] = -1
    # Tp[4, 2] = 1
    # Tp[5, 1] = -1
    # Tp[5, 3] = 1
    #
    # Tm[0, 0] = 2
    # Tm[0, 2] = -1
    # Tm[0, 4] = 1
    # Tm[1, 1] = 2
    # Tm[1, 3] = -1
    # Tm[1, 5] = 1
    # Tm[2, 0] = 2
    # Tm[2, 2] = -1
    # Tm[3, 1] = 2
    # Tm[3, 3] = -1
    # Tm[4, 0] = 1
    # Tm[4, 2] = -1
    # Tm[5, 1] = 1
    # Tm[5, 3] = -1
    #
    # T = np.zeros((16, 6, 6))
    #
    # T[0] = Tm @ Tm @ Tm @ Tm
    # T[1] = Tm @ Tm @ Tm @ Tp
    # T[2] = Tm @ Tm @ Tp @ Tm
    # T[3] = Tm @ Tm @ Tp @ Tp
    # T[4] = Tm @ Tp @ Tm @ Tm
    # T[5] = Tm @ Tp @ Tm @ Tp
    # T[6] = Tm @ Tp @ Tp @ Tm
    # T[8] = Tp @ Tm @ Tm @ Tm
    # T[9] = Tp @ Tm @ Tm @ Tp
    # T[10] = Tp @ Tm @ Tp @ Tm
    # T[12] = Tp @ Tp @ Tm @ Tm
    #
    # v = np.array([1, 0, 0, 0, 0, 1], dtype=np.float_)

    count = 0
    idx_roll = np.empty(2 * n, dtype=np.int_)
    p = np.empty(n, dtype=np.int_)
    # p = np.empty((2 * n + 1) * n, dtype=np.bool_)
    idx = idx_0
    while idx < idx_1:
        # print('idx =', idx, bin(idx))
        tmp = idx & (idx << 1) & (idx << 2)
        # print('idx =', idx, ' =', bin(idx))
        # print('tmp =', bin(tmp))
        if tmp:
            idx += tmp
            b = 0
            while tmp:
                tmp >>= 1
                b += 1
            idx = (idx >> b) << b
            # idx = (idx // tmp + 1) * tmp
            # print('new idx =', bin(idx))
        else:
            # print('idx =', idx, bin(idx))
            hw = popcount(idx)
            # p[:] = False
            if (not hw & 1) and (hw >> 2) and is_tangle(idx, p):
            # if (not hw & 1) and (hw >> 2) and is_tangle(idx, n, p, T, v):
                idx_roll[::2] = cycle_shift_right(idx)
                idx_roll[1::2] = cycle_shift_right(reverse_bit(idx))
                if idx == np.min(idx_roll[idx_roll > idx_min]):
                    count += 1
            idx += 1
    return count


start = time()
t_count = find_tangles()
print('rank:', rank, ', tangles:', t_count, ', time:', time()-start)
total_t_count = comm.reduce(t_count, op=MPI.SUM, root=0)

if rank == 0:
    print('class', c)
    print('length', n)
    print('n_tangles', total_t_count)
