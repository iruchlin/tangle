import sys
import numpy as np
import numba as nb
from numba import cuda, vectorize
from time import time
from matplotlib import pyplot as plt


def print_tangle(idx, n):
    str_b2 = '{0:0' + str(n) + 'b}'
    print(str_b2.format(idx))


@cuda.jit(device=True)
def popcount(x):
    m1 = 0x5555555555555555
    m2 = 0x3333333333333333
    m4 = 0x0f0f0f0f0f0f0f0f
    h01 = 0x0101010101010101
    x -= (x >> 1) & m1
    x = (x & m2) + ((x >> 2) & m2)
    x = (x + (x >> 4)) & m4
    return (x * h01) >> 56


@cuda.jit(device=True)
def reverse_bit(t, n):
    r = 0
    for _ in range(n):
        r = (r << 1) + (t & 1)
        t >>= 1
    return r


# @vectorize(['int64(int64, int64, int64, int64)'], target='cuda')
# def cycle_shift_right(t, j, n, m):
#     return (t >> j) | ((t << (n - j)) & m)


# @cuda.jit(device=True)
# def is_tangle(idx, p, n):
#     x, y, u, v, w = 1, 0, 0, 0, 1
#     go = n
#     ind = 1 + n * n
#     while go and not p[ind]:
#         p[ind] = True
#         a = w * (v - y)
#         b = w * (x - u)
#         if not idx & 1:
#             u = 2 * x - u
#             v = 2 * y - v
#             w = -w
#         x = a + u
#         y = b + v
#         idx >>= 1
#         go -= 1
#         ind = x + n * (y + n)
#     return x == 1 and y == 0 and u == 0 and v == 0 and w == 1 and go == 0


@cuda.jit(device=True)
def is_tangle(idx, p, n, T, v):
    p[:] = False
    v[:] = 0
    v[0] = 1
    v[5] = 1
    go = n
    ind = 1 + n * n
    while go and not p[ind]:
        p[ind] = True
        v = np.dot(T[idx & 15], v)
        idx >>= 4
        go -= 4
        ind = v[0] + n * (v[1] + n)
    return v[0] == 1 and v[1] == 0 and v[2] == 0 and v[3] == 0 and v[4] == 0 and v[5] == 1 and go == 0


# @cuda.jit()
# def find_tangles(idx_interval, count, n):
#     i = cuda.grid(1)
#
#     if i >= idx_interval.size:
#         return
#
#     p = cuda.local.array(p_length, nb.bool_)
#     local_count = 0
#
#     m = 2 ** n - 1
#     idx_min = idx_interval[0]
#     idx_max = idx_interval[-1]
#     idx = idx_interval[10*i]
#     idx_end = idx_interval[10*(i + 1)]
#     while idx < idx_end:
#         tmp = idx & (idx << 1) & (idx << 2)
#         if tmp:
#             idx += tmp
#             b = 0
#             while tmp:
#                 tmp >>= 1
#                 b += 1
#             idx = (idx >> b) << b
#         else:
#             hw = popcount(idx)
#             # p[i, :] = False
#             p[:] = False
#             # if (not hw & 1) and (hw >> 2) and is_tangle(idx, p[i], n):
#             if (not hw & 1) and (hw >> 2) and is_tangle(idx, p, n):
#                 idx_roll = idx_max
#                 xdi = reverse_bit(idx, n)
#                 for j in range(n):
#                     idx_rshift = (idx >> j) | ((idx << (n - j)) & m)
#                     if idx_roll > idx_rshift > idx_min:
#                         idx_roll = idx_rshift
#                     xdi_rshift = (xdi >> j) | ((xdi << (n - j)) & m)
#                     if idx_roll > xdi_rshift > idx_min:
#                         idx_roll = xdi_rshift
#                 if idx == idx_roll:
#                     # count[i] += 1
#                     local_count += 1
#             idx += 1
#         # total_count[i] += 1
#     count[i] = local_count


@cuda.jit()
def find_tangles(idx_interval, count, n, T):
    start = cuda.grid(1)
    stride = cuda.gridsize(1)

    # if i >= idx_interval.size:
    #     return

    p = cuda.local.array(p_length, nb.bool_)
    v = cuda.local.array(6, nb.int_)

    m = 2 ** n - 1
    idx_min = idx_interval[0]
    idx_max = idx_interval[-1]
    for i in range(start, idx_interval.shape[0], stride):
        idx = idx_interval[i]
        idx_end = idx_interval[i+1]
        local_count = 0
        while idx < idx_end:
            tmp = idx & (idx << 1) & (idx << 2)
            if tmp:
                idx += tmp
                b = 0
                while tmp:
                    tmp >>= 1
                    b += 1
                idx = (idx >> b) << b
            else:
                hw = popcount(idx)
                # p[:] = False
                # if (not hw & 1) and (hw >> 2) and is_tangle(idx, p, n):
                if (not hw & 1) and (hw >> 2) and is_tangle(idx, p, n, T, v):
                    idx_roll = idx_max
                    xdi = reverse_bit(idx, n)
                    for j in range(n):
                        idx_rshift = (idx >> j) | ((idx << (n - j)) & m)
                        if idx_roll > idx_rshift > idx_min:
                            idx_roll = idx_rshift
                        xdi_rshift = (xdi >> j) | ((xdi << (n - j)) & m)
                        if idx_roll > xdi_rshift > idx_min:
                            idx_roll = xdi_rshift
                    if idx == idx_roll:
                        local_count += 1
                idx += 1
        count[i] = local_count


def get_idx_interval(N):
    n = 24
    shift = N - n
    idx_min = 2 ** (n - 2)
    # idx_min = 0
    idx_max = int('0b' + ('011' * ((n // 3) + 1))[:n], 2)
    idx = idx_min
    tested = 0
    test_list = []
    while idx < idx_max + 1:
        # while tested < 5888:
        # while idx < 2**(n-2):
        tmp = idx & (idx << 1) & (idx << 2)
        if tmp:
            idx += tmp
            b = 0
            while tmp:
                tmp >>= 1
                b += 1
            idx = (idx >> b) << b
        else:
            # print(tangle_string(idx, n), idx)
            test_list.append(idx << shift)
            idx += 1
            tested += 1
    return np.array(test_list)


c = int(sys.argv[1])
n = 4 * c

idx_min = 2 ** (n - 2)
idx_max = int('0b' + ('011' * ((n // 3) + 1))[:n], 2) + 1

print('Using', cuda.gpus[0].name)
print('idx_min =', idx_min)
print('idx_max =', idx_max)

Tp = np.zeros((6, 6), dtype=np.int_)
Tm = np.zeros((6, 6), dtype=np.int_)

Tp[0, 2] = 1
Tp[0, 4] = 1
Tp[1, 3] = 1
Tp[1, 5] = 1
Tp[2, 2] = 1
Tp[3, 3] = 1
Tp[4, 0] = -1
Tp[4, 2] = 1
Tp[5, 1] = -1
Tp[5, 3] = 1

Tm[0, 0] = 2
Tm[0, 2] = -1
Tm[0, 4] = 1
Tm[1, 1] = 2
Tm[1, 3] = -1
Tm[1, 5] = 1
Tm[2, 0] = 2
Tm[2, 2] = -1
Tm[3, 1] = 2
Tm[3, 3] = -1
Tm[4, 0] = 1
Tm[4, 2] = -1
Tm[5, 1] = 1
Tm[5, 3] = -1

T = np.zeros((16, 6, 6), dtype=np.int_)

T[0] = Tm @ Tm @ Tm @ Tm
T[1] = Tm @ Tm @ Tm @ Tp
T[2] = Tm @ Tm @ Tp @ Tm
T[3] = Tm @ Tm @ Tp @ Tp
T[4] = Tm @ Tp @ Tm @ Tm
T[5] = Tm @ Tp @ Tm @ Tp
T[6] = Tm @ Tp @ Tp @ Tm
T[8] = Tp @ Tm @ Tm @ Tm
T[9] = Tp @ Tm @ Tm @ Tp
T[10] = Tp @ Tm @ Tp @ Tm
T[12] = Tp @ Tp @ Tm @ Tm

T_dev = cuda.to_device(T)

# threads_per_block = 16
# blocks_per_grid = 368

# threads_per_block = 32
# blocks_per_grid = 184

# threads_per_block = 184
# blocks_per_grid = 32

threads_per_block = 64
blocks_per_grid = 92

# threads_per_block = 128
# blocks_per_grid = 46

# threads_per_block = 256
# blocks_per_grid = 23

# threads_per_block = 368
# blocks_per_grid = 16

# threads_per_block = 736
# blocks_per_grid = 8

# threads_per_block = 1
# blocks_per_grid = 5888

idx_interval_host = get_idx_interval(n)
# threads_per_block = 1
# blocks_per_grid = len(idx_interval_host)
# threads_per_block = 5888 // blocks_per_grid
# threads_per_block = len(idx_interval_host)
# blocks_per_grid = 5888 // threads_per_block
print(idx_interval_host)

# count_dev = cuda.to_device(np.zeros(threads_per_block * blocks_per_grid, dtype=np.int_))
count_dev = cuda.to_device(np.zeros(idx_interval_host.shape[0], dtype=np.int_))

# total_count_dev = cuda.to_device(np.zeros(threads_per_block * blocks_per_grid, dtype=np.int_))

# idx_interval_host = np.linspace(idx_min, idx_max, threads_per_block * blocks_per_grid + 1).astype(np.int_)
idx_interval_dev = cuda.to_device(idx_interval_host)

# p = np.zeros((threads_per_block * blocks_per_grid, (2 * n + 1) * n), dtype=np.bool_)
# p_dev = cuda.to_device(p)
p_length = (2 * n + 1) * n

print('Launching CUDA kernel')

start = time()

# find_tangles[blocks_per_grid, threads_per_block](idx_interval_dev, count_dev, n, p_dev, total_count_dev)
find_tangles[blocks_per_grid, threads_per_block](idx_interval_dev, count_dev, n, T_dev)
# count_host = count_dev.copy_to_host()
# total_count_host = total_count_dev.copy_to_host()
# t_count = np.sum(count_host)
# print(count_host)
t_count = np.sum(count_dev)
print(count_dev)
# print(total_count_host)
# print('Fraction total_count==1 ', np.sum(total_count_host == 1) / len(total_count_host))

print('tangles:', t_count, ', time:', time()-start)
print('class', c)
print('length', n)
print('n_tangles', t_count)

# plt.plot(count_host, '.')
# plt.show()

# plt.plot(total_count_host, '.')
# plt.show()

