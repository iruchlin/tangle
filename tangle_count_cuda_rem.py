import sys
import numpy as np
import numba as nb
from numba import cuda
from time import time


def print_tangle(idx, n):
    str_b2 = '{0:0' + str(n) + 'b}'
    print(str_b2.format(idx))


@cuda.jit(device=True)
def popcount(x):
    m1 = 0x5555555555555555
    m2 = 0x3333333333333333
    m4 = 0x0f0f0f0f0f0f0f0f
    h01 = 0x0101010101010101
    x -= (x >> 1) & m1
    x = (x & m2) + ((x >> 2) & m2)
    x = (x + (x >> 4)) & m4
    return (x * h01) >> 56


@cuda.jit(device=True)
def reverse_bit(t, n):
    r = 0
    for _ in range(n):
        r = (r << 1) + (t & 1)
        t >>= 1
    return r


@cuda.jit(device=True)
def is_tangle(idx, p, n):
    x, y, u, v, w = 1, 0, 0, 0, 1
    go = n
    ind = 1 + n * n
    while go and not p[ind]:
        p[ind] = True
        a = w * (v - y)
        b = w * (x - u)
        if not idx & 1:
            u = 2 * x - u
            v = 2 * y - v
            w = -w
        x = a + u
        y = b + v
        idx >>= 1
        go -= 1
        ind = x + n * (y + n)
    return x == 1 and y == 0 and u == 0 and v == 0 and w == 1 and go == 0


@cuda.jit()
def find_tangles(prefix_list, prefix_shift, rem_list_not_prefix, count, n, total_count):
    tid = cuda.blockIdx.x * cuda.blockDim.x + cuda.threadIdx.x

    if tid >= prefix_shift.size:
        return

    p = cuda.local.array(p_length, nb.bool_)

    m = 2 ** n - 1
    idx_min = prefix_shift[0] + rem_list_not_prefix[0]
    idx_max = prefix_shift[-1] + rem_list_not_prefix[-1]

    local_count = 0
    local_count_total = 0

    prefix = prefix_shift[tid]
    for idx_list in [rem_list_not_prefix, prefix_list[tid:]]:
        for idx in idx_list:
            idx += prefix
            hw = popcount(idx)
            p[:] = False
            if (not hw & 1) and (hw >> 2) and is_tangle(idx, p, n):
                idx_roll = idx_max
                xdi = reverse_bit(idx, n)
                for j in range(n):
                    idx_rshift = (idx >> j) | ((idx << (n - j)) & m)
                    if idx_roll > idx_rshift > idx_min:
                        idx_roll = idx_rshift
                    xdi_rshift = (xdi >> j) | ((xdi << (n - j)) & m)
                    if idx_roll > xdi_rshift > idx_min:
                        idx_roll = xdi_rshift
                # if idx == idx_roll and is_tangle(idx, p, n):
                if idx == idx_roll:
                    local_count += 1
    count[tid] = local_count
    total_count[tid] = local_count_total


c = int(sys.argv[1])
n = 4 * c

########################################


@nb.njit()
def find_rem():
    length = c * 2
    p = np.empty(length, dtype=np.int_)
    rem = []

    for idx in range(2 ** length):
        if not idx & (idx << 1) & (idx << 2):
            x, y, u, v, w = 1, 0, 0, 0, 1
            for k in range(length):
                p[k] = x + length * y
                if idx & (1 << k):
                    x, y = u + w * (v - y), v + w * (x - u)
                else:
                    x, y, u, v, w = 2 * x - u + w * (v - y), 2 * y - v + w * (x - u), 2 * x - u, 2 * y - v, -w

            if not (x == 1 and y == 0 and u == 0 and v == 0 and w == 1) and np.unique(p).size == length:
                rem.append(idx)

    return rem


start = time()
rem_list = np.array(find_rem())

idx_min_half = 2 ** (n // 2 - 2)
idx_max_half = int('0b' + ('011' * ((n // 3) + 1))[:n//2], 2)

prefix_list_host = rem_list[(idx_min_half <= rem_list) & (rem_list <= idx_max_half)]

prefix_shift_host = prefix_list_host << (n >> 1)
rem_list_not_prefix_host = rem_list[(rem_list < prefix_list_host[0]) | (rem_list > prefix_list_host[-1])]

precompute_time = time() - start

########################################

idx_min = 2 ** (n - 2)
idx_max = int('0b' + ('011' * ((n // 3) + 1))[:n], 2) + 1

print('Using', cuda.gpus[0].name)

count_dev = cuda.to_device(np.zeros(prefix_shift_host.shape[0]-1, dtype=np.int_))

total_count_dev = cuda.to_device(np.zeros(prefix_shift_host.shape[0]-1, dtype=np.int_))

prefix_list_dev = cuda.to_device(prefix_list_host)
prefix_shift_dev = cuda.to_device(prefix_shift_host)
rem_list_not_prefix_dev = cuda.to_device(rem_list_not_prefix_host)

p_length = (2 * n + 1) * n

threads_per_block = 16
blocks_per_grid = (prefix_shift_host.size - 1 + threads_per_block - 1) // threads_per_block

print('Launching CUDA kernel')

start = time()

find_tangles[blocks_per_grid, threads_per_block](prefix_list_dev, prefix_shift_dev, rem_list_not_prefix_dev, count_dev, n, total_count_dev)

t_count = np.sum(count_dev)
total_count_host = total_count_dev.copy_to_host()

print('time', time()-start)
print('class', c)
print('length', n)
print('n_tangles', int(t_count))
