import sys
import numpy as np
import numba as nb
from time import time
from mpi4py import MPI

use_fastmath = True

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

c = int(sys.argv[1])
n = 4 * c
m = 2 ** n - 1
j = np.arange(n)


@nb.njit()
def find_rem():
    length = c * 2
    p = np.empty(length, dtype=np.int_)
    rem = []

    for idx in range(2 ** length):
        if not idx & (idx << 1) & (idx << 2):
            x, y, u, v, w = 1, 0, 0, 0, 1
            for k in range(length):
                p[k] = x + length * y
                if idx & (1 << k):
                    x, y = u + w * (v - y), v + w * (x - u)
                else:
                    x, y, u, v, w = 2 * x - u + w * (v - y), 2 * y - v + w * (x - u), 2 * x - u, 2 * y - v, -w

            if not (x == 1 and y == 0 and u == 0 and v == 0 and w == 1) and np.unique(p).size == length:
                rem.append(idx)

    return rem


start = time()
rem_list = np.array(find_rem())

idx_min_half = 2 ** (n // 2 - 2)
idx_max_half = int('0b' + ('011' * ((n // 3) + 1))[:n//2], 2)

rem_list_cut = rem_list[(idx_min_half <= rem_list) & (rem_list <= idx_max_half)]

idx_interval = np.array_split(rem_list_cut, size)
prefix_list = idx_interval[rank]

precompute_time = time() - start

idx_min = 2 ** (n - 2)
idx_max = int('0b' + ('011' * ((n // 3) + 1))[:n], 2)


def print_tangle(idx, nn):
    str_b2 = '{0:0' + str(nn) + 'b}'
    return str_b2.format(idx)


# print('idx_min =', idx_min, print_tangle(idx_min, n))
# print('idx_max =', idx_max, print_tangle(idx_max, n))


@nb.njit(fastmath=use_fastmath)
def popcount(x):
    m1 = 0x5555555555555555
    m2 = 0x3333333333333333
    m4 = 0x0f0f0f0f0f0f0f0f
    h01 = 0x0101010101010101
    x -= (x >> 1) & m1
    x = (x & m2) + ((x >> 2) & m2)
    x = (x + (x >> 4)) & m4
    return (x * h01) >> 56


@nb.njit(fastmath=use_fastmath)
def reverse_bit(t):
    r = 0
    for _ in range(n):
        r = (r << 1) + (t & 1)
        t >>= 1
    return r


@nb.njit(fastmath=use_fastmath)
def cycle_shift_right(t):
    return (t >> j) | ((t << (n - j)) & m)


# BEST SO FAR
@nb.njit(fastmath=use_fastmath)
def is_tangle(idx, p):
    x, y, u, v, w = 1, 0, 0, 0, 1
    for k in range(n):
        p[k] = x + n * y
        a = w * (v - y)
        b = w * (x - u)
        if not idx & 1:
            u = 2 * x - u
            v = 2 * y - v
            w = -w
        x = a + u
        y = b + v
        idx >>= 1
    return x == 1 and y == 0 and u == 0 and v == 0 and w == 1 and np.unique(p).size == n


@nb.njit(fastmath=use_fastmath)
def find_tangles():
    count = 0
    idx_roll = np.empty(2 * n, dtype=np.int_)
    p = np.empty(n, dtype=np.int_)

    prefix_shift = prefix_list << (n >> 1)
    rem_list_not_prefix = rem_list[(rem_list < prefix_list[0]) | (rem_list > prefix_list[-1])]

    for i, prefix in enumerate(prefix_shift):
        idx_list_1 = rem_list_not_prefix + prefix
        idx_list_2 = prefix_list[i:] + prefix
        for idx_list in [idx_list_1, idx_list_2]:
            for idx in idx_list:
                hw = popcount(idx)
                if (not hw & 1) and (hw >> 2):
                    idx_copy = idx
                    x, y, u, v, w = 1, 0, 0, 0, 1
                    for k in range(n):
                        p[k] = x + n * y
                        a = w * (v - y)
                        b = w * (x - u)
                        if not idx_copy & 1:
                            u = 2 * x - u
                            v = 2 * y - v
                            w = -w
                        x = a + u
                        y = b + v
                        idx_copy >>= 1
                    if x == 1 and y == 0 and u == 0 and v == 0 and w == 1 and np.unique(p).size == n:
                        idx_roll[:n] = cycle_shift_right(idx)
                        idx_roll[n:] = cycle_shift_right(reverse_bit(idx))
                        if idx == np.min(idx_roll[idx_roll > idx_min]):
                            count += 1
    return count


start = time()
t_count = find_tangles()
# print('rank:', rank, ', tangles:', t_count, ', time:', time()-start)
total_t_count = comm.reduce(t_count, op=MPI.SUM, root=0)

if rank == 0:
    print('class', c)
    print('length', n)
    print('n_tangles', total_t_count)
    print('precompute time', precompute_time)
    print('time', time()-start)
